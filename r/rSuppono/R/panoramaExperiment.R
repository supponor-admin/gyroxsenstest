
#' @importFrom magrittr %>%

#' @export
create_main_table <- function(pan.values, all.values, convert.to.degrees = F, window.size = 1000)
{
  dplyr::bind_rows(apply(pan.values, 1, function(t) position_summary(all.values, as.data.frame(t(t)), window.size, convert.to.degrees)))
}


#' @export
find_window <- function(data, t, window.size)
{
  index <-  data %>% with(
    which((r_pan == t$r_pan) & (r_tilt == t$r_tilt)))

  search_index <- function(data, i, t, window.size)
  {
    min_j <- max(1, i - window.size)
    max_j <- min(nrow(data), i + window.size)
    dd <- data[min_j:max_j, ]
    subset(dd, (r_pan == t$r_pan) & (r_tilt == t$r_tilt))
  }
  temp <- lapply(index, FUN = function(i) search_index(data, i, t, window.size))
  dplyr::bind_rows(temp, .id = "column_label")
}

#' @export
position_summary <- function(data, t, window.size, convert.to.degrees = F)
{
  w <- find_window(data, t, window.size)
  if (convert.to.degrees)
  {
    t$r_pan <- scale_pan(t$r_pan )
    t$r_tilt <- scale_tilt(t$r_tilt)
    w$x_pan <- scale_pan(w$x_pan )
    w$x_tilt <- scale_tilt(w$x_tilt)
  }
  data.frame(ref_pan = t$r_pan, ref_tilt = t$r_tilt,
             diff_pan = t$r_pan - median(w$x_pan), diff_tilt = t$r_tilt - median(w$x_tilt),
             min_pan = min(w$x_pan), average_pan = median(w$x_pan),
             max_pan = max(w$x_pan), sd_pan = sd(w$x_pan),
             min_tilt = min(w$x_tilt), average_tilt = median(w$x_tilt),
             max_tilt = max(w$x_tilt), sd_tilt = sd(w$x_tilt),
             average_roll = median(w$roll),n = nrow(w))
}

