#include "xsens_callback_handler.h"


#include <iostream>

#include "xspublic/xscommon/xsens_mutex.h"
#include "xspublic/xstypes/xsdatapacket.h"

using namespace dbrlive::ptzcontrol;

XsensCallbackHandler::XsensCallbackHandler(size_t maxBufferSize):
mutex(std::make_unique<xsens::Mutex>()),
      maxNumberOfPacketsInBuffer(maxBufferSize),
      numberOfPacketsInBuffer(0)
{}

XsensCallbackHandler::~XsensCallbackHandler() = default;

bool XsensCallbackHandler::packetAvailable() const
{
    xsens::Lock locky(mutex.get());
    return numberOfPacketsInBuffer > 0;
}

XsDataPacket XsensCallbackHandler::getNextPacket()
{
    xsens::Lock locky(mutex.get());
    XsDataPacket oldestPacket(packetBuffer.front());
    packetBuffer.pop_front();
    --numberOfPacketsInBuffer;
    return oldestPacket;
}

void XsensCallbackHandler::onLiveDataAvailable(XsDevice *, const XsDataPacket *packet)
{
    xsens::Lock locky(mutex.get());
    while (numberOfPacketsInBuffer >= maxNumberOfPacketsInBuffer)
        (void)getNextPacket();

    packetBuffer.push_back(*packet);
    ++numberOfPacketsInBuffer;
}
