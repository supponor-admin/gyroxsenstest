#pragma once
#include <string>
#include <memory>
#include <mutex>

struct XsPortInfo;
struct XsControl;
namespace dbrlive
{
namespace ptzcontrol
{
    class BaseXsensDevice
    {
    public:
        BaseXsensDevice();
        ~BaseXsensDevice();

        int init(const std::string& portName, const std::string& file);
        virtual std::tuple<int, int> readEuler() = 0;
        int close();
        void logPanTilt(int pan, int rawPan, int tilt, int rawTilt) const;

    protected:
        virtual int setConfiguration() = 0;

    private:
        void initLog(const std::string& file);

    protected:
        std::mutex mutex;
        std::unique_ptr<XsPortInfo> mtPortObj;
        std::string mtPort;
        XsControl* control;
    private:
        
        std::unique_ptr<std::ofstream> logFile;

        bool initAttempted;
        bool isPortOpen;
        bool isControlOpen;
    };
}
}

