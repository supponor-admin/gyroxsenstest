#pragma once
#include <string>
#include <memory>
#include <mutex>

#include "base_xsens_device.h"

struct XsControl;
struct XsDevice;
struct XsPortInfo;
namespace dbrlive
{
namespace ptzcontrol
{
class XsensCallbackHandler;
class XsensDevice : public BaseXsensDevice
{
public:
    XsensDevice();

    ~XsensDevice();

    std::tuple<int, int> readEuler() override;

protected:
    virtual int setConfiguration() override;
private:

    std::tuple<int, int> filterData(int pan, int tilt);
private:
    std::string mtPort;
    //order is sufficient for destructor
    std::unique_ptr<XsensCallbackHandler> callback;
    //control manage this
    XsDevice* device;
   
    double panDegreesToTicks;
    double tiltDegreesToTicks;

    int prevPan;
    int curPan;
    int curTilt;
    int prevTilt;

    const int maxPanDelta = 140;
    const int maxTiltDelta = 80;
};
}
}
