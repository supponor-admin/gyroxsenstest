#pragma once
#include <list>

#include "xspublic/xscontroller/xscallback.h"

struct XsDataPacket;
namespace xsens {
class Mutex;
}
namespace dbrlive
{
namespace ptzcontrol
{
class XsensCallbackHandler: public XsCallback
{
public:
    XsensCallbackHandler(size_t maxBufferSize = 5);

    virtual ~XsensCallbackHandler();

    bool packetAvailable() const;

    XsDataPacket getNextPacket();

protected:
    void onLiveDataAvailable(XsDevice*, const XsDataPacket* packet) override;
private:
    mutable std::unique_ptr<xsens::Mutex> mutex;

    size_t maxNumberOfPacketsInBuffer;
    size_t numberOfPacketsInBuffer;
    std::list<XsDataPacket> packetBuffer;
};
}
}
