
//  Copyright (c) 2003-2020 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//  
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//  
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//  
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS 
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES 
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE 
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//  

#ifndef XSSCANNER_H
#define XSSCANNER_H

#include "xstypes/xsportinfo.h"
#include "xscontrollerconfig.h"

#ifdef __cplusplus

#include <xstypes/xsstring.h>
#include <sstream>
extern "C" {
#endif
struct XsPortInfo;

//! \brief Defines the callback type that can be supplied to XsScanner_setScanLogCallback
typedef void (*XsScanLogCallbackFunc)(struct XsString const*);

XDA_DLL_API int XsScanner_scanPort(struct XsPortInfo* port, XsBaudRate baudrate, int singleScanTimeout, int detectRs485);
XDA_DLL_API void XsScanner_abortScan(void);
XDA_DLL_API void XsScanner_setScanLogCallback(XsScanLogCallbackFunc cb);

#ifdef __cplusplus
} // extern "C"

class XsScanner {
public:
	
	//! \copydoc XsScanner_scanPort
	static inline bool XSNOCOMEXPORT scanPort(XsPortInfo& port, XsBaudRate baudrate = XBR_Invalid, int singleScanTimeout = 100, bool detectRs485 = false)
	{
		return 0 != XsScanner_scanPort(&port, baudrate, singleScanTimeout, detectRs485?1:0);
	}

	/*!	\brief Scan a single port for Xsens devices.
		\param[in] portName The name of the port to scan.
		\param[in] baudrate The baudrate to scan at. When set to XBR_Invalid, all known baudrates are scanned.
		\param[in] singleScanTimeout The timeout of a scan at a single baud rate in ms.
		\param[in] detectRs485 Enable more extended scan to detect rs485 devices
		\returns An XsPortInfo structure with the results of the scan.
		\sa XsScanner_scanPort
	*/
	static inline XsPortInfo scanPort(const XsString& portName, XsBaudRate baudrate = XBR_Invalid, int singleScanTimeout = 100, bool detectRs485 = false)
	{
		XsPortInfo pi(portName, baudrate);
		if (scanPort(pi, baudrate, singleScanTimeout, detectRs485))
			return pi;

		return XsPortInfo();
	}

	/*! \brief Abort the currently running port scan(s)
		\sa XsScanner_abortScan
	*/
	static inline void abortScan(void)
	{
		XsScanner_abortScan();
	}

	/*!	\brief Set a callback function for scan log progress and problem reporting
		\details When set, any scan will use the provided callback function to report progress and failures.
		Normal operation is not affected, so all return values for the scan functions remain valid.
		\param cb The callback function to use. When set to NULL, no callbacks will be generated.
		\sa XsScanner_setScanLogCallback
	*/
	static inline void XSNOCOMEXPORT setScanLogCallback(XsScanLogCallbackFunc cb)
	{
		XsScanner_setScanLogCallback(cb);
	}
};

#endif

#endif
