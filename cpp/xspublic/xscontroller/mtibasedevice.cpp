
//  Copyright (c) 2003-2020 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//  
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//  
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//  
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS 
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES 
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE 
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//  

#include "mtibasedevice.h"
#include "xsdef.h"

#include <xstypes/xsoutputconfigurationarray.h>

#include <xstypes/xsportinfo.h>

#include <xstypes/xsquaternion.h>
#include <xstypes/xsvector.h>
#include <set>


using namespace xsens;

/*! \brief Constructs a device
	\param comm The communicator to construct with
*/
MtiBaseDevice::MtiBaseDevice(Communicator* comm)
	: MtDeviceEx(comm)
{
}

/*! \brief Constructs a child device for a master device
	\param master The master device to construct for
*/
MtiBaseDevice::MtiBaseDevice(XsDevice* master)
	: MtDeviceEx(master, XsDeviceId())
{
}

MtiBaseDevice::~MtiBaseDevice()
{
}

/*! \copybrief XsDevice::setHeadingOffset
*/
bool MtiBaseDevice::setHeadingOffset(double)
{
	return false;
}

/*! \copybrief XsDevice::outputConfiguration
*/
XsOutputConfigurationArray MtiBaseDevice::outputConfiguration() const
{
	XsMessage snd(XMID_ReqOutputConfiguration), rcv;
	if (!doTransaction(snd, rcv))
		return XsOutputConfigurationArray();

	XsOutputConfigurationArray rv;
	XsSize count = rcv.getDataSize() >> 2;
	for (XsSize row = 0; row < count; row++)
	{
		uint16_t dataId, freq;
		dataId = rcv.getDataShort(row * 4);
		freq = rcv.getDataShort(2 + row * 4);
		rv.push_back(XsOutputConfiguration((XsDataIdentifier)dataId, freq));
	}

	return rv;
}

/*! \brief Set the output configuration for this device
	\param config The desired output configuration
	\returns true if the output configuration was successfully updated
*/
bool MtiBaseDevice::setOutputConfiguration(XsOutputConfigurationArray& config)
{
	if (!deviceId().isMti6X0())
		setStringOutputMode(0, 0, 0);

	return MtDeviceEx::setOutputConfiguration(config);
}

/*! \copybrief XsDevice::setAlignmentRotationMatrix
*/
bool MtiBaseDevice::setAlignmentRotationQuaternion(XsAlignmentFrame frame, const XsQuaternion& quat)
{
	XsMessage snd(XMID_SetAlignmentRotation, 1 + XS_LEN_ALIGNMENTROTATION);
	snd.setBusId(busId());
	snd.setDataByte((uint8_t)frame);
	for (XsSize i = 0; i < 4; ++i)
		snd.setDataFloat((float)quat[i], (uint16_t) (1 + i * sizeof(float)));
	return doTransaction(snd);
}

/*! \copybrief XsDevice::alignmentRotationQuaternion
*/
XsQuaternion MtiBaseDevice::alignmentRotationQuaternion(XsAlignmentFrame frame) const
{
	XsQuaternion quat;
	XsMessage snd(XMID_ReqAlignmentRotation), rcv;
	snd.setDataByte(frame);
	if (doTransaction(snd, rcv))
	{
		for (XsSize i = 0; i < 4; ++i)
			quat[i] = rcv.getDataFloat(1 + i * 4);
	}
	return quat;
}


/*! \returns the update rate for the specified XsDataIdentifier in the configuration list or 0 if no such data available
	\param dataType The data identifier for which the update rate is requested
	\param configurations The configuration list in which the specified dataType must be looked up
*/
int MtiBaseDevice::calculateUpdateRateImp(XsDataIdentifier dataType, const XsOutputConfigurationArray& configurations) const
{
	int matchLevel = 0;
	int result = 0;

	bool groupCheck = ((dataType & XDI_TypeMask) == dataType);
	for (XsOutputConfigurationArray::const_iterator i = configurations.begin(); i != configurations.end(); ++i)
	{
		int ml = 0;
		if ((dataType & XDI_FullTypeMask) == (i->m_dataIdentifier & XDI_FullTypeMask))
		{
			if (dataType == i->m_dataIdentifier)
				ml = 3;
			else
				ml = 2;
		}
		else if (groupCheck && (dataType == (i->m_dataIdentifier & XDI_TypeMask)))
			ml = 1;

		if (ml > matchLevel)
		{
			result = i->m_frequency;
			if (ml == 3)
				break;
			matchLevel = ml;
		}
	}

	return result;
}

/*! \returns the update rate for the specified XsDataIdentifier or 0 if no such data available
	\details This function only looks at the output configuration configured in the device, not XDA calculated data
	\param dataType The data identifier to use
	\sa calculateUpdateRateImp
*/
int MtiBaseDevice::calculateUpdateRate(XsDataIdentifier dataType) const
{
	return calculateUpdateRateImp(dataType, outputConfiguration());
}

/*! \returns the base update rate (Hz) corresponding to the dataType. Returns 0 if no update rate is available
	\param dataType The data identifier to use
*/
int MtiBaseDevice::getBaseFrequency(XsDataIdentifier dataType) const
{
	return getBaseFrequencyInternal(dataType).m_frequency;
}

/*! \copybrief XsDevice::supportedUpdateRates
*/
std::vector<int> MtiBaseDevice::supportedUpdateRates(XsDataIdentifier dataType) const
{
	std::vector<int> updateRates;
	auto baseFreq = getBaseFrequencyInternal(dataType);

	if (baseFreq.m_frequency == 0)
		return updateRates;

	if (!baseFreq.m_divedable)
	{
		updateRates.push_back(baseFreq.m_frequency);
		return updateRates;
	}

	std::set<int> unsupportedUpdateRates;
	unsupportedUpdateRates.insert(500);
	unsupportedUpdateRates.insert(250);
	unsupportedUpdateRates.insert(125);

	for (int skip = 0; skip <= baseFreq.m_frequency; ++skip)
	{
		int freq = calcFrequency(baseFreq.m_frequency, (uint16_t) skip);
		if (freq * (skip+1) == baseFreq.m_frequency)
		{
			if (unsupportedUpdateRates.count(freq) == 0)
				updateRates.push_back(freq);
		}
	}

	return updateRates;
}

/*! \copybrief XsDevice::setNoRotation
*/
bool MtiBaseDevice::setNoRotation(uint16_t duration)
{
	return MtDeviceEx::setNoRotation(duration);
}

/*! \copybrief XsDevice::setInitialPositionLLA
*/
bool MtiBaseDevice::setInitialPositionLLA(const XsVector& lla)
{
	uint8_t bid = (uint8_t) busId();
	if (bid == XS_BID_INVALID || bid == XS_BID_BROADCAST || lla.size() != 3)
		return false;

	XsMessage snd(XMID_SetLatLonAlt, 3*sizeof(double));
	snd.setDataDouble(lla[0], 0);
	snd.setDataDouble(lla[1], 8);
	snd.setDataDouble(lla[2], 16);
	snd.setBusId(bid);

	return doTransaction(snd);
}

/*! \copybrief XsDevice::utcTime
*/
XsTimeInfo MtiBaseDevice::utcTime() const
{
	uint8_t bid = (uint8_t) busId();
	if (bid == XS_BID_INVALID || bid == XS_BID_BROADCAST)
		return XsTimeInfo();

	XsMessage snd(XMID_ReqUtcTime, 0), rcv;
	snd.setBusId(bid);

	if (!doTransaction(snd, rcv))
		return XsTimeInfo();

	XsTimeInfo time;
	time.m_nano = rcv.getDataLong(0);
	time.m_year = rcv.getDataShort(4);
	time.m_month = rcv.getDataByte(6);
	time.m_day = rcv.getDataByte(7);
	time.m_hour = rcv.getDataByte(8);
	time.m_minute = rcv.getDataByte(9);
	time.m_second = rcv.getDataByte(10);
	time.m_valid = rcv.getDataByte(11);
	time.m_utcOffset = 0;

	return time;
}

/*! \copybrief XsDevice::setUtcTime
*/
bool MtiBaseDevice::setUtcTime(const XsTimeInfo& time)
{
	uint8_t bid = (uint8_t) busId();
	if (bid == XS_BID_INVALID || bid == XS_BID_BROADCAST)
		return false;

	XsMessage snd(XMID_SetUtcTime, XS_LEN_UTCTIME);
	snd.setDataLong(time.m_nano, 0);
	snd.setDataShort(time.m_year, 4);
	snd.setDataByte(time.m_month, 6);
	snd.setDataByte(time.m_day, 7);
	snd.setDataByte(time.m_hour, 8);
	snd.setDataByte(time.m_minute, 9);
	snd.setDataByte(time.m_second, 10);
	snd.setDataByte(time.m_valid, 11);
	snd.setBusId(bid);

	if (!doTransaction(snd))
		return false;

	return true;
}

/*! \copybrief XsDevice::rs485TransmissionDelay
*/
uint16_t MtiBaseDevice::rs485TransmissionDelay() const
{
	XsMessage snd(XMID_ReqTransmitDelay), rcv;
	if (!doTransaction(snd, rcv))
		return 0;
	return rcv.getDataShort();
}

/*! \copybrief XsDevice::setRs485TransmissionDelay
*/
bool MtiBaseDevice::setRs485TransmissionDelay(uint16_t delay)
{
	return delay == 0;
}

/*! \copybrief XsDevice::resetRemovesPort
*/
bool MtiBaseDevice::resetRemovesPort() const
{
	Communicator* comm = communicator();
	if (!comm)
		return false;

	bool removesPort = false;
	uint16_t vid, pid;
	comm->portInfo().getVidPid(vid, pid);
	// Direct connection, COM port will be removed
	if (vid == XSENS_VENDOR_ID && pid < 0x00FF)
		removesPort = true;

	return removesPort;
}

bool MtiBaseDevice::messageLooksSane(const XsMessage &msg) const
{
	(void)msg;
	return true;
}

/*! \returns True if device uses on board filtering */
bool MtiBaseDevice::deviceUsesOnBoardFiltering()
{
	return updateRateForDataIdentifier(XDI_OrientationGroup) > 0;
}

/*! \returns True if this device has an ICC support
*/
bool MtiBaseDevice::hasIccSupport() const
{
	return (firmwareVersion() >= XsVersion(1, 5, 0));
}

void MtiBaseDevice::fetchAvailableHardwareScenarios()
{
	if (deviceId().isImu())								// If we are a 100 type device,
		m_hardwareFilterProfiles.clear();				// there are no filter profiles in the firmware.
	else												// For other device types,
		MtDeviceEx::fetchAvailableHardwareScenarios();	// fetch the scenarios.
}
