
//  Copyright (c) 2003-2020 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//  
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//  
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//  
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS 
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES 
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE 
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//  

#include "scanner.h"

#ifdef _WIN32
#	include <devguid.h>
#	include <initguid.h>
#	include <cfgmgr32.h>
#	include <regstr.h>
#	include <regex>
#	include "idfetchhelpers.h"
#include "SetupAPI.h"
#else
#	include <stdlib.h>
#	include <string.h>
#	include <dirent.h>
#	include "xslibusb.h"
#	include "udev.h"
#endif
#include <algorithm>

#include "serialportcommunicator.h"

#include <iostream>
#include <atomic>

#include <xstypes/xsbusid.h>
#include <xstypes/xsportinfo.h>


namespace XsScannerNamespace {
	volatile std::atomic_bool abortPortScan{false};
	Scanner* gScanner = nullptr;
	XsScanLogCallbackFunc gScanLogCallback = nullptr;
}
using namespace XsScannerNamespace;

/*!	\class Scanner::Accessor
	\brief An accessor class for scanner
*/

/*! \returns The reference to a scanner object
*/
Scanner& Scanner::Accessor::scanner() const
{
	if (!gScanner)
		gScanner = new Scanner();
	return *gScanner;
}

/*!	\class Scanner
	\brief Provides static functionality for scanning for Xsens devices.
*/

/*! \brief Destructor */
Scanner::~Scanner()
{
}

/*!	\brief Set a callback function for scan log progress and problem reporting
	\details When set, any scan will use the provided callback function to report progress and failures.
	Normal operation is not affected, so all return values for the scan functions remain valid.
	\param cb The callback function to use. When set to NULL, no callbacks will be generated.
*/
void Scanner::setScanLogCallback(XsScanLogCallbackFunc cb)
{
	gScanLogCallback = cb;
}

/*!	\brief Fetch basic device information

	\param[in, out] portInfo  The name of the port to fetch from
	\param[in] singleScanTimeout The timeout of a scan of a single port at a single baud rate in ms.
	\param[in] detectRs485 Enable more extended scan to detect rs485 devices

	\returns XRV_OK if successful
*/
XsResultValue Scanner::fetchBasicInfo(XsPortInfo &portInfo, uint32_t singleScanTimeout, bool detectRs485)
{
	auto serial = Communicator::createUniquePtr<SerialPortCommunicator>();

	SerialPortCommunicator *port = serial.get();

	port->setGotoConfigTimeout(singleScanTimeout);
	LOGXSSCAN("Opening port " << portInfo.portName() << " (" << portInfo.portNumber() << ") @ " << portInfo.baudrate() << " baud, expected device " << portInfo.deviceId());
	if (!port->openPort(portInfo, OPS_OpenPort))
	{
		LOGXSSCAN("Failed to open port because: " << XsResultValue_toString(port->lastResult()));
		return port->lastResult();
	}

	if (!port->openPort(portInfo, OPS_InitStart, detectRs485))
	{
		LOGXSSCAN("Failed to initialize port because: " << XsResultValue_toString(port->lastResult()));
		if (port->lastResult() < XRV_ERROR)
		{
			LOGXSSCAN("Attempting to reset device");
			// we got an xbus protocol error message, attempt to reset the device and try again (once)
			XsMessage snd(XMID_Reset);
			snd.setBusId(XS_BID_MASTER);
			if (!port->writeMessage(snd))
			{
				LOGXSSCAN("Failed to wriite reset to device because: " << XsResultValue_toString(port->lastResult()));
				return port->lastResult();
			}
			LOGXSSCAN("Reopening port after reset");
			port->closePort();
			XsTime::msleep(2000);
			if (!port->openPort(portInfo, OPS_Full, detectRs485))
			{
				LOGXSSCAN("Failed to reopen port after reset because: " << XsResultValue_toString(port->lastResult()));
				return port->lastResult();
			}
		}
		else
			return port->lastResult();
	}

	LOGXSSCAN("Port " << portInfo.portName() << " opened successfully, device is " << port->masterDeviceId());
	portInfo.setDeviceId(port->masterDeviceId());

	// Enable flow control for Awinda2 stations/dongles which support this:
	if (port->masterDeviceId().isAwinda2())
	{
		XsPortLinesOptions portLinesOptions = portInfo.linesOptions();
		XsVersion fwVersion = port->firmwareRevision();
		XsVersion hwVersion = port->hardwareRevision();

		if (port->masterDeviceId().isAwinda2Station() && fwVersion.major() != 255 && fwVersion >= XsVersion(4, 2, 1))
			portLinesOptions = (XsPortLinesOptions)(portLinesOptions | XPLO_RtsCtsFlowControl);
		else if (port->masterDeviceId().isAwinda2Dongle() && fwVersion.major() != 255 && fwVersion >= XsVersion(4, 3, 2) && hwVersion >= XsVersion(2, 3))
			portLinesOptions = (XsPortLinesOptions)(portLinesOptions | XPLO_RtsCtsFlowControl);
		else
			portLinesOptions = (XsPortLinesOptions)(portLinesOptions & ~XPLO_RtsCtsFlowControl);
		portInfo.setLinesOptions(portLinesOptions);
	}

	return XRV_OK;
}

/*!	\brief Scan a single COM port for connected Xsens devices

	\details The xsScanPort function will scan a single port for connected Xsens devices. If the
	baudrate parameter is 0 (default), it will try to connect at all supported baud
	rates, starting with the most common 115k2, 460k8 and 58k6. If the baudrate parameter
	is non-zero, only the specified baud rate is tried. Any detected devices are returned
	in the portInfo parameter.

	\param[in, out] portInfo The name of the port to scan should be in this parameter, the other contents will be filled by the function
	\param[in] baud The baudrate to scan at. When set to XBR_Invalid, all known baudrates are scanned
	\param[in] singleScanTimeout The timeout of a scan of a single port at a single baud rate in ms
	\param[in] detectRs485 Enable more extended scan to detect rs485 devices

	\returns true if a device was found, false otherwise
*/
bool Scanner::xsScanPort(XsPortInfo& portInfo, XsBaudRate baud, uint32_t singleScanTimeout, bool detectRs485)
{
	LOGXSSCAN("Scanning port " << portInfo.portName() << " at baudrate " << baud << " with timeout " << singleScanTimeout << " detectRs485 " << detectRs485);
	XsResultValue res;
	XsBaudRate baudrate;

	if (baud == 0)
		baudrate = XBR_115k2;
	else
		baudrate = baud;

	while (!abortPortScan)
	{
		portInfo.setBaudrate(baudrate);
		res = fetchBasicInfo(portInfo, singleScanTimeout, detectRs485);
		if (res == XRV_OK)
		{
			LOGXSSCAN("Scan successfully found device " << portInfo.deviceId() << " on port " << portInfo.portName());
			portInfo.setBaudrate(baudrate);
			return true;
		}

		// failed, determine if we need to scan other baudrates or not
		if (res != XRV_TIMEOUT && res != XRV_TIMEOUTNODATA && res != XRV_CONFIGCHECKFAIL)
		{
			LOGXSSCAN("Failed to fetch basic info: " << XsResultValue_toString(res));
			return false;
		}
		LOGXSSCAN("Failed to fetch basic info within timeout");

		// not detected, try next baudrate
		if (baud != 0)
		{
			LOGXSSCAN("Failed to find device");
			return false;
		}
		switch(baudrate)
		{
		default:
		case XBR_115k2:
			baudrate = XBR_2000k; break;
		case XBR_2000k:
			baudrate = XBR_921k6; break;
		case XBR_921k6:
			baudrate = XBR_460k8; break;
		case XBR_460k8:
			baudrate = XBR_230k4; break;
		case XBR_230k4:
			// On some systems a delay of about 100ms seems necessary to successfully perform the scan.
			XsTime::msleep(100);
			baudrate = XBR_57k6; break;
		case XBR_57k6:
			baudrate = XBR_38k4; break;
		case XBR_38k4:
			baudrate = XBR_19k2; break;
		case XBR_19k2:
			baudrate = XBR_9600; break;
		case XBR_9600:
			LOGXSSCAN("No more available baudrates, failed to find device");
			return false;	// could not detect Xsens device, return false
		}
		LOGXSSCAN("Checking next baudrate: " << baudrate);
	}
	LOGXSSCAN("Port scan aborted by external trigger");
	return false;
}

#ifdef _WIN32
/*! \returns The device path for given windows device
	\param[in] hDevInfo The refernce to a device information
	\param[in] DeviceInfoData The pointer to a device information data
*/
std::string Scanner::getDevicePath(HDEVINFO hDevInfo, SP_DEVINFO_DATA *DeviceInfoData)
{
	char deviceInstanceID[MAX_DEVICE_ID_LEN];
	SetupDiGetDeviceInstanceIdA(hDevInfo, DeviceInfoData,deviceInstanceID, MAX_DEVICE_ID_LEN, NULL);
	return std::string(deviceInstanceID);
}
#endif

/*! \returns true if the vendor/product combination may point to an xsens device
	\param[in] vid The vendor ID
	\param[in] pid the product ID
*/
bool Scanner::isXsensUsbDevice(uint16_t vid, uint16_t pid)
{
	switch (vid)
	{
	// Xsens
	case XSENS_VENDOR_ID:
		// ignore the body pack serial port
		return (pid != 0x0100);

	// FTDI reserved PIDs
	case FTDI_VENDOR_ID:
		return (pid >= 0xd388 && pid <= 0xd38f);

	default:
		return false;
	}
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

#define ILLEGAL_HUB	(0)
#ifdef _WIN32
#define HUB_SEARCH_STRING ("Hub_#")

/*!	\brief Retrieves the USB Hub number for a device
	\param[in] hDevInfo The refernce to a device information
	\param[in] deviceInfoData The pointer to a device information data
	\returns Non-zero if successful
*/
int Scanner::xsScanGetHubNumber(HDEVINFO hDevInfo, SP_DEVINFO_DATA *deviceInfoData)
{
	DWORD DataT;
	char buffer[256];
	int result = ILLEGAL_HUB;

	if (SetupDiGetDeviceRegistryPropertyA(hDevInfo,
			deviceInfoData,
			SPDRP_LOCATION_INFORMATION,
			&DataT,
			(PBYTE)buffer,
			256,
			NULL))
	{
		LOGXSSCAN("Registry access successful: \"" << buffer << "\"");
		char const * hubString = strstr((char const *)buffer, HUB_SEARCH_STRING);
		if (hubString)
		{
			result = strtol(hubString + strlen(HUB_SEARCH_STRING), 0, 10);
		}
	}
	else
		LOGXSSCAN("Get Hub Number failed with error " << GetLastError());

	return result;
}

#endif // _WIN32
