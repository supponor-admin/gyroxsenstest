#include "xsens_device.h"

#include "xsens_callback_handler.h"
#include "xstypes/xsoutputconfigurationarray.h"
#include "xspublic/xstypes/xsportinfo.h"
#include "xspublic/xstypes/xsdatapacket.h"
#include "xspublic/xscontroller/xscontrol_def.h"
#include "xspublic/xscontroller/xsdevice_def.h"

using namespace dbrlive::ptzcontrol;

XsensDevice::XsensDevice(): 
    callback(std::make_unique<XsensCallbackHandler>()),
    panDegreesToTicks(-2e6 / 360.0),
    tiltDegreesToTicks(1e6 / 360.0)
{}

XsensDevice::~XsensDevice()
{
    close();
    //control.reset();
    callback.reset();
}


int XsensDevice::setConfiguration()
{
    device = control->device(mtPortObj->deviceId());

    device->addCallbackHandler(callback.get());

    // Put the device into configuration mode before configuring the device
    if (!device->gotoConfig())
        return close();

    // Important for Public XDA!
    // Call this function if you want to record a mtb file:
    //device->readEmtsAndDeviceConfiguration();

    XsOutputConfigurationArray configArray;
    configArray.push_back(XsOutputConfiguration(XDI_PacketCounter, 0));
    configArray.push_back(XsOutputConfiguration(XDI_SampleTimeFine, 0));
    if (!device->deviceId().isAhrs())
        return close();

    configArray.push_back(XsOutputConfiguration(XDI_Quaternion, 0));
    if (!device->setOutputConfiguration(configArray))
    {
        return close();
    }

    if (!device->gotoMeasurement())
        return close();
    return 0;
}


namespace
{
std::tuple<int, int> notInRangeEuler()
{
    return std::make_tuple(-181, -181);
}

int toTicks(const double degree, const double degreesToTicks)
{
    return static_cast<int>(std::round(degree * degreesToTicks));
}

int checkWithDelta(int& prevValue, int& curValue, int delta, int value)
{
    if (std::abs(value - prevValue) <= delta)
    {
        prevValue = value;
        return curValue;
    }
    prevValue = value;
    curValue = value;
    return value;
}
}


std::tuple<int, int> XsensDevice::filterData(int pan, int tilt)
{
    const auto returnPan = checkWithDelta(prevPan, curPan, maxPanDelta, pan);
    const auto returnTilt = checkWithDelta(prevTilt, curTilt, maxTiltDelta, tilt);
    return std::make_tuple(returnPan, returnTilt);
}

std::tuple<int, int> XsensDevice::readEuler()
{
    const std::lock_guard<std::mutex> lock(mutex);
    if (!callback->packetAvailable())
    {
        return notInRangeEuler();
    }
    const auto packet = callback->getNextPacket();
    const auto euler = packet.orientationEuler();
    auto tilt = toTicks(euler.roll(), tiltDegreesToTicks),
         pan = toTicks(euler.yaw(), panDegreesToTicks);

    std::tie(pan, tilt) = filterData(pan, tilt);
    return std::make_tuple(pan, tilt);
}

