#include <iostream>
#include <string>

#include "xspublic/xstypes/xstime.h"

#include "xsens_device.h"


void test()
{
    auto xDev = std::make_unique<dbrlive::ptzcontrol::XsensDevice>();
    xDev->init("COM3", "");
    auto i = 0;
    while (i < 15)
    {
        XsTime::msleep(100);
        int a, b;
        std::tie(a, b) = xDev->readEuler();
        std::cout << a << " " << b << std::endl;
        i++;
    }
}

int main(void)
{
    test();
}
