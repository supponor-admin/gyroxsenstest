#include "base_xsens_device.h"

#include <fstream>

#include "xspublic/xscontroller/xsscanner.h"
#include "xspublic/xscontroller/xscontrol_def.h"

using namespace dbrlive::ptzcontrol;

BaseXsensDevice::BaseXsensDevice() :
    initAttempted(false),
    isPortOpen(false), isControlOpen(false)
{}

BaseXsensDevice::~BaseXsensDevice()
{
    
}

int BaseXsensDevice::init(const std::string &portName, const std::string& file)
{
    if (initAttempted)
        return 1;
    const std::lock_guard<std::mutex> lock(mutex);
    if (portName.empty())
    {
        initLog(file);
        return 1;
    }
    initAttempted = true;
    //control = std::unique_ptr<XsControl>(XsControl::construct());
    control = XsControl::construct();
    isControlOpen = true;
    mtPortObj = std::make_unique<XsPortInfo>(XsScanner::scanPort(portName, XBR_Invalid,
        0, true));
    mtPort = mtPortObj->portName().toStdString();
    if (mtPortObj->empty() || !mtPortObj->deviceId().isMti())
        return close();

    if (!control->openPort(mtPort, mtPortObj->baudrate()))
        return close();
    isPortOpen = true;

    if (setConfiguration())
        return close();
    initLog(file);
    return 1;
}

void BaseXsensDevice::logPanTilt(int pan, int rawPan, int tilt, int rawTilt) const
{
    if (logFile != nullptr)
    {
        *logFile << rawPan << "," << pan << "," << rawTilt << "," << tilt << std::endl;
    }
}


void BaseXsensDevice::initLog(const std::string& file)
{
    if (file.empty())
        return;
    logFile = std::make_unique<std::ofstream>();
    logFile->open(file);
    *logFile << "r_pan, x_pan, r_tilt, x_tilt" << std::endl;
}

int BaseXsensDevice::close()
{
    if (logFile != nullptr)
        logFile->close();

    if (isPortOpen)
    {
        control->closePort(mtPort);
        isPortOpen = false;
    }
    if (isControlOpen)
    {
        control->destruct();
        isControlOpen = false;
    }
    return 1;
}
